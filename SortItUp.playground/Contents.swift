import UIKit

///Giving an array arr, consisting of only zeroes, ones and twos
///Sort the same array in-place and return it. Do not create a new array.

func sortItUp(_ arr: [Int]) -> [Int]{
    return arr.sorted { lhs, rhs in
        lhs < rhs
    }
}

sortItUp([2,0,1,0,2])
sortItUp([2,2,2,0,0,0,0,2,2,2,0,0,1,1,0,1,2,2,0,1,1,1])
